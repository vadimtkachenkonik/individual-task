﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.VisualBasic.Devices;
using Microsoft.VisualBasic;
using System.Runtime.InteropServices;

namespace WpfApp5
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("winmm.dll", EntryPoint = "mciSendStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int mciSendString(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Record_Click(object sender, RoutedEventArgs e)
        {
            // Запись с микрофона
            
            mciSendString("open new Type waveaudio Alias recsound", "", 0, 0);
            mciSendString("set recsound bitspersample 16", "", 0, 0);
            mciSendString("set recsound samplespersec 44100", "", 0, 0);
            mciSendString("record recsound", "", 0, 0);
            DoubleAnimation da = new DoubleAnimation();
            da.From = 75;
            da.To = 150;
            da.Duration = TimeSpan.FromSeconds(1);
            
            Record.BeginAnimation(Button.WidthProperty, da);
        }
    

        private void SaveStop_Click(object sender, RoutedEventArgs e)
        {
            // Остнановка записи и сохранение
            mciSendString("save recsound c:\\record.wav", "", 0, 0);
            // аудио сохранено в корень диска С
            mciSendString("close recsound ", "", 0, 0);
            Computer c = new Computer();
            c.Audio.Stop();

            DoubleAnimation da = new DoubleAnimation();
            da.From = 150;
            da.To = 75;
            da.Duration = TimeSpan.FromSeconds(1);
            Record.BeginAnimation(Button.WidthProperty, da);
            


        }

        private void Read_Click(object sender, RoutedEventArgs e)
        {
            // Воспроизведение
            Computer computer = new Computer();
            computer.Audio.Play("c:\\record.wav", AudioPlayMode.Background);
            
        }

        
    }
}
